#include "ppos.h"
#include "queue.h"


task_t *tarefas_fila;
task_t * current_task;
task_t dispatcher_task;
int start_id; // id de uma tarefa nova;

int user_tasks = 0;

void dispatcher();


// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void ppos_init () {
    /* desativa o buffer da saida padrao (stdout), usado pela função printf */
    setvbuf (stdout, 0, _IONBF, 0) ;

    /* --- Cria a fila de tarefas --- */
    //fprintf(stderr, "INFO: &current_task = %p [ppos_init1]\n", current_task);


    /* --- Cria tarefa main e insere na fila --- */
    task_t main_task;
    //main_task = (task_t*) calloc(1, sizeof(task_t));

    getcontext(&main_task.context);
    start_id = 0; // 0 = main
    main_task.id = start_id;

    //queue_append((queue_t**)&tarefas_fila, (queue_t*)&main_task); // insere main na fila

    /* --- Cria tarefa dispatcher e insere na fila --- */
    //fprintf(stderr, "INFO: &dispatcher_task = %p [ppos_init2]\n", &dispatcher_task);
    
    task_create(&dispatcher_task, (void *)dispatcher, "Dispatcher    !");

    //fprintf(stderr, "INFO: &current_task = %p [ppos_init3]\n", current_task);
    //queue_append((queue_t**)&tarefas_fila, (queue_t*)&main_task); // insere dispatcher na fila

    current_task = &main_task;

    //fprintf(stderr, "INFO: &current_task.context = %p [ppos_init]\n", &current_task->context);
    //fprintf(stderr, "GOOD: function ended [ppos_init]\n");

}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,		// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg) {			// argumentos para a tarefa

    // Pega o contexto
    // Cria a stack
    // Preenche o contexto
    // makecontext
    // insere na fila

    if(!task) return 0;

    getcontext (&task->context) ;
    //fprintf(stderr, "\nINFO: creating %s [task_create1]\n", (char*)arg);

    char* stack = malloc (STACKSIZE) ;
    if (stack)
    {
        //fprintf(stderr, "INFO: [task_create2]\n");
        
        task->prev = NULL;
        task->next = NULL;
        
        if(queue_append((queue_t**)&tarefas_fila, (queue_t*) task)){
            fprintf(stderr, "ERR: cannot create task[task_create]\n");
            return -1;
        }
        //fprintf(stderr, "INFO: [task_create22]\n");

        start_id ++;
        task->id = start_id;
        task->status = NOVA;

        task->context.uc_stack.ss_sp = stack ;
        task->context.uc_stack.ss_size = STACKSIZE ;
        task->context.uc_stack.ss_flags = 0 ;
        task->context.uc_link = 0 ;

        task->preemptable = 1;

    //fprintf(stderr, "INFO: [task_create3]\n");

        if(!start_func) return 0;

        makecontext (&task->context, (void*)(*start_func), 1, arg) ;

    //fprintf(stderr, "INFO: [task_create4]\n");
    }
    else
    {
        perror ("Erro na criação da pilha: ") ;
        exit (1) ;
    }
    //fprintf(stderr, "INFO: [task_create5]\n");

    task->status = PRONTA;
    user_tasks++;
    
    //fprintf(stderr, "GOOD: function ended [task_create] context = %p(id%d)\n\n", &task->context, task->id);

    return task->id;
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exit_code) {
    user_tasks--;
    tarefas_fila->status = TERMINADA;
    task_switch(&dispatcher_task);
}

// alterna a execução para a tarefa indicada
int task_switch (task_t *task) {
    current_task->status = PRONTA;

    task_t *trash = current_task;

    current_task = task;
    //fprintf(stderr, "                                               WAR: 1 [task_switch] -> on %p (id%d) to %p\n", &trash->context, task_id(), &task->context);

    task->status = RODANDO;

    int r = swapcontext(&trash->context, &task->context);
    //fprintf(stderr, "                                               WAR: 2 [task_switch]\n");

    
    //fprintf(stderr, "GOOD: function ended [task_switch]\n\n");

    return r;
}

// retorna o identificador da tarefa corrente (main deve ser 0)
int task_id () {
    return current_task->id;
}

// suspende a tarefa atual na fila "queue"
void task_suspend (task_t **queue) {
    tarefas_fila->status = SUSPENSA;
    task_yield(); // Não tenho tanta certeza dissoooo
}

// acorda a tarefa indicada, que está suspensa na fila indicada
void task_resume (task_t *task, task_t **queue) {

}

// operações de escalonamento ==================================================

task_t* scheduler(){ 
    // Caminha até a próxima tarefa PRONTA da fila
    task_t* ultima = tarefas_fila; // Representa que já foi dado a volta inteira na fila e nào há tarefas PRONTAS

    while(tarefas_fila->next != ultima){
        //fprintf(stderr, "WAR: [scheduler] Verificando %d (%d)\n", tarefas_fila->next->id, tarefas_fila->next->status);
        tarefas_fila = tarefas_fila->next;  // Aponta para a próxima tarefa 
        if(tarefas_fila->status == PRONTA){ // Verifica se ela está pronta
            return tarefas_fila;
        }
    }   // Verificar todas menos a primeira

    if (tarefas_fila->status != PRONTA) 
        return NULL; // Se chegar aqui, não existe nenhuma tarefa PRONTA

    return tarefas_fila; // Se chegar aqui, existe apenas uma tarefa PRONTA, e ela está no fim/começo da fila
}


void dispatcher(){
    if(user_tasks <= 0)
        task_exit(0);

    while (tarefas_fila){
        task_t* next_task = scheduler();

        if(next_task){
            task_switch(next_task);
        }
    }
    // Verifica quem é o próximo com o scheduler
    // faz switch pra ele
    // Caso não existam tarefas terminar o programa
}

// libera o processador para a próxima tarefa, retornando à fila de tarefas
// prontas ("ready queue")
void task_yield () {
    task_switch(&dispatcher_task);
    //fprintf(stderr, "GOOD: function ended [task_yield]\n");

}