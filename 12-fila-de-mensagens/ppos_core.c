#include "ppos.h"
#include <signal.h>
#include <sys/time.h>
#include <strings.h>

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

long global_tick;
#define GLOBAL_QUANTUM 20

// Estruturas de tarefas 
task_t *fila_tarefas, *fila_suspensas, *fila_dormindo;
task_t *current_task, main_task;
task_t dispatcher_task;
int start_id; // id de uma tarefa nova;

int user_tasks = 0;

void dispatcher();
void preempt_handler(int signum);
unsigned int systime () ;


// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void ppos_init () {


    /* desativa o buffer da saida padrao (stdout), usado pela função printf */
    setvbuf (stdout, 0, _IONBF, 0) ;

    /* --- Cria tarefa main manualmente --- */

    getcontext(&main_task.context);
    start_id = 0; // 0 = main
    main_task.id = start_id;
    main_task.level = USER_LEVEL;
    main_task.quantum = GLOBAL_QUANTUM;
    main_task.cpu_time = 0;
    main_task.exec_time = systime();
    main_task.activation_count = 0;
    main_task.joined_task = NULL;
    main_task.exit_code = __INT_MAX__;
    
    if(queue_append((queue_t**)&fila_tarefas, (queue_t*) &main_task));
    current_task = &main_task;    

    /* --- Cria tarefa dispatcher e insere na fila --- */
    
    task_create(&dispatcher_task, (void *)dispatcher, "Dispatcher    !");
    dispatcher_task.level = KERNEL_LEVEL;

    //


    // registra a ação para o sinal de timer SIGALRM
    action.sa_handler = preempt_handler ;
    sigemptyset (&action.sa_mask) ;
    action.sa_flags = 0 ;
    if (sigaction (SIGALRM, &action, 0) < 0)
    {
        perror ("Erro em sigaction: ") ;
        exit (1) ;
    }

    // ajusta valores do temporizador
    timer.it_value.tv_usec = 10 ;      // primeiro disparo, em micro-segundos
    timer.it_value.tv_sec = 0; // em segundos
    timer.it_interval.tv_usec = 1000 ;   // disparos subsequentes, em micro-segundos
    timer.it_interval.tv_sec = 0; // em segundos

    // arma o temporizador ITIMER_REAL (vide man setitimer)
    if (setitimer (ITIMER_REAL, &timer, 0) < 0)
    {
        perror ("Erro em setitimer: ") ;
        exit (1) ;
    }

    /* Inicia o Timer */

    global_tick = 0;

}

// tratador do sinal
void preempt_handler(int signum){
    global_tick ++;
    if(signum == SIGALRM){
        if(current_task->level == USER_LEVEL){

            if(current_task->quantum -- <= 0){
                task_yield();
            }
        }
    }
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,		// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg) {			// argumentos para a tarefa

    if(!task) return -1;
    if(!start_func) return -1;

    char* stack = calloc (1, STACKSIZE) ;
    if (stack)
    {
        /* -- Preenche o contexto -- */
        getcontext (&task->context) ;
        task->context.uc_stack.ss_sp = stack ;
        task->context.uc_stack.ss_size = STACKSIZE ;
        task->context.uc_stack.ss_flags = 0 ;
        task->context.uc_link = 0 ;
        makecontext (&task->context, (void*)(*start_func), 1, arg);
        
        /* -- Preenche info da task e insere na fila de tarefas de usuário -- */
        start_id++;
        task->id = start_id;
        task->status = NOVA;
        task->preemptable = 1;
        task->prio_life = 0;
        task->prio_static = 0;
        task->quantum = GLOBAL_QUANTUM;
        task->level = USER_LEVEL;
        task->cpu_time = 0;
        task->exec_time = systime();
        task->exit_code = 0;
        task->joined_task = NULL;

        task->prev = NULL;
        task->next = NULL;

        if(queue_append((queue_t**)&fila_tarefas, (queue_t*) task)){
            
            return -1;
        }
    }
    else
    {
        perror ("Erro na criação da pilha: ") ;
        exit (1) ;
    }

    task->status = PRONTA;
    user_tasks++;
    
    return task->id;
}

// Caminha entre as suspensas liberando as que podem ser liberadas
int varre_suspensas(){
    if(!fila_suspensas) return 1;

    task_t* ultima = fila_suspensas; // Representa que já foi dado a volta inteira na fila e nào há tarefas PRONTAS
    task_t* elem;
    do{ // Dará o loop completo

        fila_suspensas = fila_suspensas->next; // Aponta para a próxima tarefa 
        

        if(fila_suspensas->joined_task->status == TERMINADA){
            elem = fila_suspensas;
            
            task_resume(elem, &fila_suspensas);
            ultima = fila_suspensas;
        }

        if(!fila_suspensas) return 0;
    }while(fila_suspensas != ultima);// Verificar todas menos a primeira

 
    return 0;
}

int varre_dormindo(){
    if(!fila_dormindo) return 1;
    //

    task_t* ultima = fila_dormindo; // Representa que já foi dado a volta inteira na fila e nào há tarefas PRONTAS
    task_t* elem;
    do{ // Dará o loop completo
    
        fila_dormindo = fila_dormindo->next; // Aponta para a próxima tarefa 
    
        if(systime() > fila_dormindo->sleep_until){  // Verifica tempo da tarefa
            elem = fila_dormindo;
            
            
            task_resume(elem, &fila_dormindo);
            ultima = fila_dormindo;
        }

        if(!fila_dormindo) return 0;
    }while(fila_dormindo != ultima);// Verificar todas menos a primeira

    return 0;
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exit_code) {
    user_tasks--;
    current_task->status = TERMINADA;

    queue_remove((queue_t**)&fila_tarefas, (queue_t*) current_task);

    current_task->exit_code = exit_code;
    current_task->exec_time = systime() - current_task->exec_time; // t2 - t1

    fprintf(stdout, "Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", 
                    current_task->id, 
                    current_task->exec_time,
                    current_task->cpu_time, 
                    current_task->activation_count);

    task_switch(&dispatcher_task);

}

// alterna a execução para a tarefa indicada
int task_switch (task_t *task) {   
    if(!current_task){
        task_t aux;
        current_task = &aux;
    }

    if(current_task == task) return 0;

    task_t *trash = current_task;

    current_task = task;
    task->status = RODANDO;

    // Volta prioridade e quantum originais da task que volta pra fila
    task->prio_life = task->prio_static;
    task->quantum = GLOBAL_QUANTUM;

    task->activation_count++;
    int r = swapcontext(&trash->context, &task->context);
    

    return r;
}

// retorna o identificador da tarefa corrente (main deve ser 0)
int task_id () {
    if(current_task)
        return current_task->id;
    return -1;
}


// operações de escalonamento ==================================================

task_t* scheduler(){ 
    varre_dormindo();
    varre_suspensas();

    if(!fila_tarefas){

        return NULL;
    }

    task_t* ultima = fila_tarefas; // Representa que já foi dado a volta inteira na fila e nào há tarefas PRONTAS
    task_t* the_chosen_one = ultima; // Será a com maior prioridade
    int maior_prio = 21; // 20 é a menor prioridade, enquando -20 é a maior
    

   // Caminha até a próxima tarefa PRONTA da fila
    do{ // Dará o loop completo
        if(fila_tarefas->prio_life > -20)   // Aging
            fila_tarefas->prio_life --;

        fila_tarefas = fila_tarefas->next;              // Aponta para a próxima tarefa 
        if(fila_tarefas->status == PRONTA)              // Verifica se ela está pronta
            if(fila_tarefas->prio_life < maior_prio){   // Verifica se ela tem mais prioridade
                maior_prio = fila_tarefas->prio_life;
                the_chosen_one = fila_tarefas;
            }
        
    }while(fila_tarefas != ultima);// Verificar todas menos a primeira

    fila_tarefas = the_chosen_one; // A fila agora aponta para a escolhida
    //
    
    return the_chosen_one;
}


void dispatcher(){
    // Caso não existam tarefas usuário terminar o programa
    // Verifica quem é o próximo com o scheduler
    // faz switch pra ele

    while (user_tasks > 0){
        task_t* next_task = scheduler();

        if(next_task){
            unsigned int t1 = systime();
            task_switch(next_task);
            next_task->cpu_time += systime() - t1; // t2 - t1
        }
    }
    task_exit(0);
}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio) {
    if (!task)
        task = current_task;

    if (task->prio_life < prio)
        task->prio_life = prio;
    
    task->prio_static = prio;
}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task) {
    if(!task)    
        task = current_task;
    return current_task->prio_static;
}

// libera o processador para a próxima tarefa, retornando à fila de tarefas
// prontas ("ready queue")
void task_yield () {
    task_switch(&dispatcher_task);
}


// suspende a tarefa atual na fila "queue"
void task_suspend (task_t **queue) {
    current_task->status = SUSPENSA;
    queue_remove((queue_t**)&fila_tarefas, (queue_t*)current_task);  
    queue_append((queue_t**)queue, (queue_t*)current_task);
    task_yield();
}

// acorda a tarefa indicada, que está suspensa na fila indicada
void task_resume (task_t *task, task_t **queue) {
    if(queue){
        queue_remove((queue_t**)queue, (queue_t*)task);
        
        task->status = PRONTA;
        queue_append((queue_t**)&fila_tarefas, (queue_t*)task);
    }
}

// a tarefa corrente aguarda o encerramento de outra task
int task_join (task_t *task) {
    if(!task) return -1;


    if(task->status != TERMINADA) {
        current_task->joined_task = task;
        
        task_suspend(&fila_suspensas);
    }
    return task->exit_code;
}

// operações de gestão do tempo ================================================

// suspende a tarefa corrente por t milissegundos
void task_sleep (int t) {

    current_task->sleep_until = systime() + t -1;
    
    task_suspend(&fila_dormindo);
}

// retorna o relógio atual (em milisegundos)
unsigned int systime () {
    return global_tick;
}


// operações de semaforo ================================================

// cria um semáforo com valor inicial "value"
int sem_create (semaphore_t *s, int value) {
    s->lock = 0;
    s->counter = value;
    s->fila = NULL;
    s->status = S_ACTIVE;

    return 1;
}


void enter_cs(int *lock){
    while (__sync_fetch_and_or(lock, 1));
}

void leave_cs(int *lock){
    (*lock) = 0;
}

// requisita o semáforo
int sem_down (semaphore_t *s) {
    if(!s) return -1;
    if(!s->status) return -1;

    enter_cs(&s->lock);
    s->counter --;

    if(s->counter < 0){
        leave_cs(&s->lock);
        task_suspend(&s->fila);
    }
    leave_cs(&s->lock);
    return 0;
}

// libera o semáforo
int sem_up (semaphore_t *s) {
    if(!s) return -1;
    if(!s->status) return -1;
    


    enter_cs(&s->lock);

    s->counter ++;
    if(s->counter <= 0){
        task_resume(s->fila, &s->fila);
    }

    leave_cs(&s->lock);
    return 0;
}

// destroi o semáforo, liberando as tarefas bloqueadas
int sem_destroy (semaphore_t *s) {
    if(!s) return -1;
    if(!s->status) return -1;

    while(s->fila){
        s->fila->exit_code = -1;
        sem_up(s);
    }
    return 0;
}

// operações de fila de mensagem ================================================

// cria uma fila para até max mensagens de size bytes cada
int mqueue_create (mqueue_t *queue, int max, int size) {
    if(!queue || max <= 0 || size <= 0) return -1;


    queue->buffer = calloc(1, max);
    free (calloc(1, 1));

    if(!queue->buffer) return -1;
    
    queue->max = max;
    queue->msize = size;
    queue->qnt_msg = 0;
    
    if (! sem_create(&queue->s_item, 0) ) return -1;
    if (! sem_create(&queue->s_vaga, 1) ) return -1;
    if (! sem_create(&queue->s_buffer, 1) ) return -1;

    queue->read_pos = queue->buffer;
    queue->write_pos = queue->buffer;
    queue->last_pos = queue->buffer + (queue->max) ;


    return 0;
}

// Printa buffer de fila de int ou double
void print_buffer(mqueue_t *queue){
    int* a;
    double *b;
    if(queue->msize == 4){
        fprintf(stderr, "\nEntre %p - %p  ->%d pos de tam %d: (", queue->buffer, queue->last_pos, queue->max, queue->msize);
        for(void* i = queue->buffer; i < queue->last_pos; i+=8 ){
            a = i;
            fprintf(stderr, "%d - ", *a);
        }

        fprintf(stderr, ") R: %p W:%p\n", queue->read_pos, queue->write_pos);

    } else {
        fprintf(stderr, "\nEntre %p - %p  ->%d pos de tam %d: (", queue->buffer, queue->last_pos, queue->max, queue->msize);
        for(void* i = queue->buffer; i < queue->last_pos; i+= 8 ){
            b = i;
            fprintf(stderr, "%f - ", *b);
        }
        fprintf(stderr, ")\n");
    }

}


// envia uma mensagem para a fila
int mqueue_send (mqueue_t *queue, void *msg) { // Anda com o write

    if(!queue) return -1;
    if(!queue->buffer) return -1;
    if(!queue->msize)  return -1;


    if (! sem_down(&queue->s_vaga) ) return -1;
    if (! sem_down(&queue->s_buffer) ) return -1;

    // Writer não pode chegar andando no reader. Reader pode chegar no writer

    int count_time = 0; 
    while((queue->write_pos + 8) == queue->read_pos){
        task_sleep(1000);
        count_time++;
        if(count_time > MAX_WAIT) 
            return -1;
    }   // Caso no próximo passo ele alcance, então não anda

    
    if(!queue->msize) return -1;
    
    bcopy(msg, queue->write_pos, queue->msize);
    

    queue->write_pos += 8; // Anda com o ponteiro
    if(queue->write_pos == queue->last_pos)
        queue->write_pos = queue->buffer; // Volta para a primeira pos caso necessario


    queue->qnt_msg ++;

    if (! sem_up(&queue->s_buffer) ) return -1;
    if (! sem_up(&queue->s_item) ) return -1;



    return 0;
}

// recebe uma mensagem da fila
int mqueue_recv (mqueue_t *queue, void *msg) {  // Anda com o read
   
    if(!queue) return -1;
    if(!queue->buffer) return -1;
    if(!queue->msize)  return -1;


    if (! sem_down(&queue->s_item) ) return -1;
    if (! sem_down(&queue->s_buffer) ) return -1;

    int count_time =0;
    while((queue->read_pos) == queue->write_pos){
        task_sleep(1000);
        count_time++;
        if(count_time > MAX_WAIT) 
            return -1;
    } // Caso estejam no mesmo lugar, não ande
        //  busy waiting
    
    if(!queue->msize) return -1;

    bcopy(queue->read_pos, msg, queue->msize);

    queue->read_pos += 8;
    if(queue->read_pos == queue->last_pos)
        queue->read_pos = queue->buffer; // Volta para a primeira pos caso necessario

    queue->qnt_msg --;

    if (! sem_up(&queue->s_buffer) ) return -1;
    if (! sem_up(&queue->s_vaga) ) return -1;

    return 0;
}

// destroi a fila, liberando as tarefas bloqueadas
int mqueue_destroy (mqueue_t *queue) {
    if(!queue) return -1;

    sem_destroy(&queue->s_buffer);
    sem_destroy(&queue->s_item);
    sem_destroy(&queue->s_vaga);
    if(queue->buffer)
        free(queue->buffer);

    queue->msize = 0; // Indica que não podem ser mais enviadas mensagens

    return 1;
}

// informa o número de mensagens atualmente na fila
int mqueue_msgs (mqueue_t *queue) {
    if(!queue) return -1;
    return queue->qnt_msg;
}