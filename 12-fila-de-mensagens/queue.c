//  EDUARDO ROSSO BARBOSA GRR20190378

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

//------------------------------------------------------------------------------
// Conta o numero de elementos na fila
// Retorno: numero de elementos na fila
int queue_size (queue_t *queue) {
    if(queue == NULL){
        //fprintf(stderr, "WAR: Fila vazia [QUEUE_SIZE]\n");
        return 0;

    }

    queue_t *e_ini = queue;
    queue_t *el = e_ini;

    int cont = 0;

    do{
        if(el->next == NULL){
            //fprintf(stderr, "ERR: Fila corrompida; Elemento sem próximo. [QUEUE_SIZE]\n");
            return 0;
        }
        el = el->next;
        cont++;
    }while (el != e_ini);

    //fprintf(stderr, "WAR: Fila de tamanho %d. [QUEUE_SIZE]\n", cont);

    return cont;
}

//------------------------------------------------------------------------------
// Percorre a fila e imprime na tela seu conteúdo. A impressão de cada
// elemento é feita por uma função externa, definida pelo programa que
// usa a biblioteca. Essa função deve ter o seguinte protótipo:
//
// void print_elem (void *ptr) ; // ptr aponta para o elemento a imprimir
void queue_print (char *name, queue_t *queue, void print_elem (void*) ) {
    if(queue == NULL){
        //fprintf(stderr, "WAR: Fila vazia [QUEUE_SIZE]\n");
        fprintf(stdout, "[");
        print_elem(NULL);
        fprintf(stdout, "]\n");
        return;
    }

    queue_t *e_ini = queue;
    queue_t *el = e_ini;

    fprintf(stdout, "[");
    do{
        print_elem(el);

        if(el->next == NULL){
            ////fprintf(stderr, "ERR: Fila corrompida; Elemento sem próximo. [QUEUE_SIZE]\n");
            return;
        }
        
        el = el->next;
        
        if(el != e_ini)
            fprintf(stdout, " ");
    }while (el != e_ini);
    fprintf(stdout, "]\n");
    
}

//------------------------------------------------------------------------------
// Insere um elemento no final da fila.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - o elemento deve existir
// - o elemento nao deve estar em outra fila
// Retorno: 0 se sucesso, <0 se ocorreu algum erro

int queue_append (queue_t **queue, queue_t *elem) {

    if(elem == NULL){
        //fprintf(stderr, "ERR: [QUEUE_APPEND] Elemento null\n");
        return 1;
    }
    if(elem->next != NULL || elem->prev != NULL){
        //fprintf(stderr, "ERR: [QUEUE_APPEND] Elemento não isolado\n");
        return 2;
    }

    if(*queue == NULL){
        *queue = elem;
        elem->next = elem;
        elem->prev = elem;
        //fprintf(stderr, "WAR: [QUEUE_APPEND] Inserido em fila vazia\n");

        return 0;
    }

    elem->next = *queue;
    elem->prev = (*queue)->prev;

    (*queue)->prev->next = elem;
    (*queue)->prev = elem;

    //fprintf(stderr, "WAR: [QUEUE_APPEND] Inserido normalmente\n");
    
    return 0;
}

//------------------------------------------------------------------------------
// Remove o elemento indicado da fila, sem o destruir.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - a fila nao deve estar vazia
// - o elemento deve existir
// - o elemento deve pertencer a fila indicada
// Retorno: 0 se sucesso, <0 se ocorreu algum erro

int queue_remove (queue_t **queue, queue_t *elem) {
    if(queue == NULL || elem == NULL){
        //fprintf(stderr, "\n ERR: [QUEUE_REMOVE] Fila ou elemento null\n");
        return 1;
    }
    if(elem->next == NULL || elem->prev == NULL){
        //fprintf(stderr, "\n ERR: [QUEUE_REMOVE] Elemento inconsistente\n");
        return 2;
    }


    queue_t *e_ini = *queue;
    queue_t *el = e_ini;
    
    // Verifica se elemento está nessa fila

    while (el != elem){
        if(el->next == NULL){
            //fprintf(stderr, "ERR: [QUEUE_REMOVE] Fila corrompida; Elemento sem próximo.\n");
            return 3;
        }
        el = el->next;
        if(el == e_ini){
            //fprintf(stderr, "ERR: [QUEUE_REMOVE] Elemento não está nessa fila. \n");
            return 4;
        }
    }

    // Verifica se elemento a se remover é o primeiro da lista

    if (elem == e_ini){
        *queue = elem->next;
    }

    // Verifica se fila possui apenas um elemento e este está sendo removido

    if (elem->next == elem){
        *queue = NULL;
    }

    elem->next->prev = elem->prev;
    elem->prev->next = elem->next;
    
    elem->next = NULL;
    elem->prev = NULL;

    return 0;
}
