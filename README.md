# PingPongOS

Projeto semestral para a matéria de sistemas operacionais. Sistema Operacional simplificado.

Operacional System's course project. Simplified OS for educational purposes.

## Information

Developed by Eduardo R. Barbosa

Based on classes by Carlos Maziero

Full course can be found on:
https://wiki.inf.ufpr.br/maziero/doku.php?id=so:start

## Concepts

Concepts required for developing:

- Generic Queue Library
- Processes
- Threads
- Escalation Algorythms
- Scheduling
- Posix Messages 
- Busy Waiting
- Semaphores
- Conflit Handling problems (producer-consumer, readers-writers; Dinning-philosophers)

