#include "ppos.h"

int start_id; // id de uma tarefa nova;

task_t *current_task; // task que está rodando no momento
task_t *main_task; // task da main

// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void ppos_init () {
    /* desativa o buffer da saida padrao (stdout), usado pela função printf */
    setvbuf (stdout, 0, _IONBF, 0) ;

    start_id = 0; // 0 = main

    current_task = (task_t*) calloc(1, sizeof(task_t));
    main_task = (task_t*) calloc(1, sizeof(task_t));

    getcontext(&main_task->context);
    main_task->id = start_id;

    current_task = main_task;
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg) {			// argumentos para a tarefa

    // Pega o contexto
    // Cria a stack
    // Preenche o contexto
    // makecontext

    if(!task) return 0;
    getcontext (&task->context) ;

    char* stack = malloc (STACKSIZE) ;
    if (stack)
    {
        start_id ++;
        task->id = start_id;
        task->status = NOVA;

        task->context.uc_stack.ss_sp = stack ;
        task->context.uc_stack.ss_size = STACKSIZE ;
        task->context.uc_stack.ss_flags = 0 ;
        task->context.uc_link = 0 ;

        task->prev = NULL;
        task->next = NULL;
        task->preemptable = 1;


        if(!start_func) return 0;

        makecontext (&task->context, (void*)(*start_func), 1, arg) ;

    }
    else
    {
        perror ("Erro na criação da pilha: ") ;
        exit (1) ;
    }

    task->status = PRONTA;
    
    return task->id;
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exit_code) {
    current_task = main_task;
    setcontext(&main_task->context);
}

// alterna a execução para a tarefa indicada
int task_switch (task_t *task) {

    task_t *trash = current_task;

    current_task = task;

    int r = swapcontext(&trash->context, &task->context);
    
    return r;
}

// retorna o identificador da tarefa corrente (main deve ser 0)
int task_id () {
    return current_task->id;
}

// suspende a tarefa atual na fila "queue"
void task_suspend (task_t **queue) {

}

// acorda a tarefa indicada, que está suspensa na fila indicada
void task_resume (task_t *task, task_t **queue) {

}