// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DINF UFPR
// Versão 1.4 -- Janeiro de 2022

// Estruturas de dados internas do sistema operacional

#ifndef __PPOS_DATA__
#define __PPOS_DATA__

#include <ucontext.h>		// biblioteca POSIX de trocas de contexto

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

#define NOVA 1        // Status das tasks
#define PRONTA 2
#define RODANDO 3
#define SUSPENSA 4
#define TERMINADA 0

#define STACKSIZE 64*1024	/* tamanho de pilha das threads */

#define USER_LEVEL 1
#define KERNEL_LEVEL 0

// Estrutura que define um Task Control Block (TCB)
typedef struct task_t
{
  struct task_t *prev, *next ;		// ponteiros para usar em filas
  int id ;				// identificador da tarefa
  ucontext_t context ;			// contexto armazenado da tarefa
  short status ;			// pronta, rodando, suspensa, ...
  short preemptable ;			// pode ser preemptada?
  int prio_static;
  int prio_life;
  int quantum;
  short level; // 0 = KERNEL, 1 = USER
  unsigned int cpu_time; // Tempo de uso de processador
  unsigned int exec_time; // Tempo total da vida da task
  unsigned int activation_count; // Contagem de ativação
  int exit_code;  // Quando finalizada, preencher o return_code;
  struct task_t *joined_task; //Task com a qual esta task foi juntada
  unsigned int sleep_until; // Momento em que a task pode ser acordada
   // ... (outros campos serão adicionados mais tarde)
} task_t ;

// estrutura que define um semáforo
typedef struct
{
  int lock;
  int counter;
  task_t* fila;
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  // preencher quando necessário
} mqueue_t ;

#endif
