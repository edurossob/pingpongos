// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DINF UFPR
// Versão 1.4 -- Janeiro de 2022

// Teste de semáforos (pesado)

#include <stdio.h>
#include <stdlib.h>
#include "ppos.h"

#define NUMVAGAS 5


// fila de int
typedef struct buffer_t {
  struct task_t *prev, *next ;		// ponteiros para usar em filas
  int item ;				// identificador da tarefa
} buffer_t;

buffer_t* fila_buffer;

semaphore_t  s_vaga ;
semaphore_t  s_buffer ;
semaphore_t  s_item ;



// corpo dos produtores
void produtor(void *id)
{
  while (1) {
    task_sleep(1000);
    
    buffer_t elem;
    elem.item = (rand() % 100);

    fprintf(stdout, "%s produziu %d\n", (char *)id, elem.item);

    sem_down(&s_vaga);
    sem_down(&s_buffer);
    queue_append((queue_t**) &fila_buffer, (queue_t*)&elem);
    sem_up(&s_buffer);
    sem_up(&s_item);
  }

  task_exit (0) ;
}

// corpo dos consumidores
void consumidor(void *id)
{
  while (1) {
    sem_down(&s_item);
    sem_down(&s_buffer);
    buffer_t* elem = fila_buffer;
    queue_remove((queue_t**) &fila_buffer, (queue_t*)elem);
    sem_up(&s_buffer);
    sem_up(&s_vaga);

    fprintf(stdout, "                   %s consumiu %d\n", (char *)id, elem->item);
    task_sleep(1000);
  }

  task_exit (0) ;
}

int main (int argc, char *argv[])
{


  printf ("main: inicio\n") ;

  ppos_init () ;

  // inicializa semáforos em 0 (bloqueado)
  sem_create (&s_vaga, NUMVAGAS) ;
  sem_create (&s_item, 0) ;
  sem_create (&s_buffer, 1) ;


  // cria produtores
  task_t p1, p2, p3;
  task_create (&p1, produtor, "p1") ;
  task_create (&p2, produtor, "p2") ;
  task_create (&p3, produtor, "p3") ;


  task_t c1, c2, c3;
  // cria consumidores
  task_create (&c1, consumidor, "c1") ;
  task_create (&c2, consumidor, "c2") ;
  task_create (&c3, consumidor, "c3") ;



  task_exit (0) ;
  exit (0) ;
}