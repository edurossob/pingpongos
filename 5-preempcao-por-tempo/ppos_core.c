#include "ppos.h"
#include "queue.h"

#include <signal.h>
#include <sys/time.h>

// estrutura que define um tratador de sinal (deve ser global ou static)
struct sigaction action ;

// estrutura de inicialização to timer
struct itimerval timer;

long global_tick;
#define GLOBAL_QUANTUM 20

// Estruturas de tarefas 
task_t *tarefas_fila, * current_task, main_task;
task_t dispatcher_task;
int start_id; // id de uma tarefa nova;

int user_tasks = 0;

void dispatcher();
void preempt_handler(int signum);


// Inicializa o sistema operacional; deve ser chamada no inicio do main()
void ppos_init () {


    /* desativa o buffer da saida padrao (stdout), usado pela função printf */
    setvbuf (stdout, 0, _IONBF, 0) ;

    /* --- Cria tarefa main --- */

    getcontext(&main_task.context);
    start_id = 0; // 0 = main
    main_task.id = start_id;
    main_task.level = USER_LEVEL;

    current_task = (task_t*) calloc(1, sizeof(task_t));
    current_task = &main_task;
    current_task->quantum = GLOBAL_QUANTUM;

    /* --- Cria tarefa dispatcher e insere na fila --- */
    
    task_create(&dispatcher_task, (void *)dispatcher, "Dispatcher    !");
    dispatcher_task.level = KERNEL_LEVEL;

    //fprintf(stderr, "GOOD: function ended [ppos_init]\n");

    /* Inicia o Timer */

    global_tick = 0;

    // registra a ação para o sinal de timer SIGALRM
    action.sa_handler = preempt_handler ;
    sigemptyset (&action.sa_mask) ;
    action.sa_flags = 0 ;
    if (sigaction (SIGALRM, &action, 0) < 0)
    {
        perror ("Erro em sigaction: ") ;
        exit (1) ;
    }

    // ajusta valores do temporizador
    timer.it_value.tv_usec = 10 ;      // primeiro disparo, em micro-segundos
    timer.it_value.tv_sec = 0; // em segundos
    timer.it_interval.tv_usec = 1000 ;   // disparos subsequentes, em micro-segundos
    timer.it_interval.tv_sec = 0; // em segundos

    // arma o temporizador ITIMER_REAL (vide man setitimer)
    if (setitimer (ITIMER_REAL, &timer, 0) < 0)
    {
        perror ("Erro em setitimer: ") ;
        exit (1) ;
    }


}

void preempt_handler(int signum){
    global_tick ++;
    if(signum == SIGALRM){
        if(current_task->level == USER_LEVEL){

            if(current_task->quantum -- <= 0){
                task_yield();
            }
        }
    }
}

// gerência de tarefas =========================================================

// Cria uma nova tarefa. Retorna um ID> 0 ou erro.
int task_create (task_t *task,		// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg) {			// argumentos para a tarefa

    if(!task) return -1;
    if(!start_func) return -1;

    char* stack = calloc (1, STACKSIZE) ;
    if (stack)
    {
        /* -- Preenche o contexto -- */
        getcontext (&task->context) ;
        task->context.uc_stack.ss_sp = stack ;
        task->context.uc_stack.ss_size = STACKSIZE ;
        task->context.uc_stack.ss_flags = 0 ;
        task->context.uc_link = 0 ;
        makecontext (&task->context, (void*)(*start_func), 1, arg);
        
        /* -- Preenche info da task e insere na fila de tarefas de usuário -- */
        start_id++;
        task->id = start_id;
        task->status = NOVA;
        task->preemptable = 1;
        task->prio_life = 0;
        task->prio_static = 0;
        task->quantum = GLOBAL_QUANTUM;
        task->level = USER_LEVEL;

        task->prev = NULL;
        task->next = NULL;

        if(queue_append((queue_t**)&tarefas_fila, (queue_t*) task)){
            fprintf(stderr, "ERR: cannot append task[task_create]\n");
            return -1;
        }
    }
    else
    {
        perror ("Erro na criação da pilha: ") ;
        exit (1) ;
    }

    task->status = PRONTA;
    user_tasks++;
    
    return task->id;
}

// Termina a tarefa corrente, indicando um valor de status encerramento
void task_exit (int exit_code) {
    
    if(current_task->id == 0){
            fprintf(stderr, "ERR: SAINDO DA MAIN [task_exit]\n");
    }
   
    user_tasks--;
    tarefas_fila->status = TERMINADA;

    queue_remove((queue_t**)&tarefas_fila, (queue_t*) current_task);

    task_switch(&dispatcher_task);

}

// alterna a execução para a tarefa indicada
int task_switch (task_t *task) {   
    if(!current_task){
        task_t aux;
        current_task = &aux;
    }

    current_task->status = PRONTA;    

    task_t *trash = current_task;

    current_task = task;
    task->status = RODANDO;

    // Volta prioridade e quantum originais da task que volta pra fila
    task->prio_life = task->prio_static;
    task->quantum = GLOBAL_QUANTUM;

    int r = swapcontext(&trash->context, &task->context);
    
    //fprintf(stderr, "GOOD: function ended [task_switch]\n\n");

    return r;
}

// retorna o identificador da tarefa corrente (main deve ser 0)
int task_id () {
    if(current_task)
        return current_task->id;
    return -1;
}

// suspende a tarefa atual na fila "queue"
void task_suspend (task_t **queue) {
    tarefas_fila->status = SUSPENSA;
    task_yield();
}

// acorda a tarefa indicada, que está suspensa na fila indicada
void task_resume (task_t *task, task_t **queue) {

}

// operações de escalonamento ==================================================

task_t* scheduler(){ 
    // Caminha até a próxima tarefa PRONTA da fila
    task_t* ultima = tarefas_fila; // Representa que já foi dado a volta inteira na fila e nào há tarefas PRONTAS
    task_t* the_chosen_one = ultima; // Será a com maior prioridade
    int maior_prio = 21; // 20 é a menor prioridade, enquando -20 é a maior
    

    do{ // Dará o loop completo
        if(tarefas_fila->prio_life > -20)   // Aging
            tarefas_fila->prio_life --;

        tarefas_fila = tarefas_fila->next;              // Aponta para a próxima tarefa 
        if(tarefas_fila->status == PRONTA)              // Verifica se ela está pronta
            if(tarefas_fila->prio_life < maior_prio){   // Verifica se ela tem mais prioridade
                maior_prio = tarefas_fila->prio_life;
                the_chosen_one = tarefas_fila;
            }
        
    }while(tarefas_fila != ultima);// Verificar todas menos a primeira

    tarefas_fila = the_chosen_one; // A fila agora aponta para a escolhida

    return the_chosen_one;
}


void dispatcher(){
    // Caso não existam tarefas usuário terminar o programa
    // Verifica quem é o próximo com o scheduler
    // faz switch pra ele

    if(user_tasks <= 0)
        task_exit(0);

    while (tarefas_fila){
        task_t* next_task = scheduler();

        if(next_task == &dispatcher_task){
            task_exit(0);
        }

        if(next_task){
            task_switch(next_task);
        }
    }
}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio) {
    if (!task)
        task = current_task;

    if (task->prio_life < prio)
        task->prio_life = prio;
    
    task->prio_static = prio;
}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task) {
    if(!task)    
        task = current_task;
    return current_task->prio_static;
}

// libera o processador para a próxima tarefa, retornando à fila de tarefas
// prontas ("ready queue")
void task_yield () {
    task_switch(&dispatcher_task);
}